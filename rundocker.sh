#!/bin/bash

if [ $# -lt 3 ]; then
    echo "Usage $0 IMAGEID SOURCE TEST [TEST...]"
    exit 1
fi

IMAGEID=$1
shift

DID=$(docker run --rm -td --network $DOCKERNETWORK -v $DATASETSVOLUME:/datasets:ro $IMAGEID)
# echo Got docker running on $DID

# copy source file
docker cp "$1" $DID:/sources
shift

while (( "$#" )); do
    docker cp "$1" $DID:/tests
    shift
done

RESULTF=$(mktemp "/tmp/zeppelingrade-result.XXXXXX")

timeout -k 10 $RUNTIMEOUT docker exec $DID bash /runme.sh > $RESULTF
STATUS=$?

docker stop $DID 2> /dev/null > /dev/null

if [ $STATUS -eq 0 ]
then
    cat $RESULTF
    rm $RESULTF
    exit 0
elif [ $STATUS -eq 124 ]
    echo "{\"messages\": [\"Took too long to run (timeout after $RUNTIMEOUT seconds)\"], \"compiled\": false}"
    rm $RESULTF
    exit 0
then
    cat timeout.json
    rm $RESULTF
    exit $STATUS
fi

