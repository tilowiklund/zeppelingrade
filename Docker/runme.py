#!/bin/env python
import argparse
import json
import requests
import time
import os
import re
from collections import defaultdict

parser = argparse.ArgumentParser(description='Run tests on Scala source')
parser.add_argument('-a', '--host', default='localhost', dest='host')
parser.add_argument('-p', '--port', default='8080', dest='port')
parser.add_argument( 'source', metavar='Source',
                     help='File containing source code' )
parser.add_argument( 'tests', metavar='Test', nargs='+',
                     help='JSON test files to run' )
args = parser.parse_args()

host = "http://{}:{}".format(args.host, args.port)

def new_notebook(name):
    requestURL  = "{}/api/notebook".format(host)
    requestJSON = {"name" : name}
    r = requests.post(requestURL, json=requestJSON).json()
    if r["status"] == "CREATED":
        return r["body"]
    else:
        raise IOError(str(r))

def delete_notebook(notebook):
    requestURL  = "{}/api/notebook/{}".format(host,notebook)
    r = requests.delete(requestURL).json()
    if not r["status"] == "OK":
        raise IOError(str(r))

def export_notebook(notebook):
    requestURL  = "{}/api/notebook/export/{}".format(host, notebook)
    r = requests.get(requestURL).json()
    return r

def add_paragraph(notebook, title, text):
    requestURL  = "{}/api/notebook/{}/paragraph".format(host, notebook)
    requestJSON = {"title" : title, "text" : text}
    r = requests.post(requestURL, json=requestJSON).json()
    if r["status"] == "CREATED":
        return (notebook, r["body"])
    else:
        raise IOError(str(r))

def delete_paragraph(paragraph):
    (notebook, paragraph_id) = paragraph
    requestURL = "{}/api/notebook/{}/paragraph/{}".format(host, notebook, paragraph_id)
    r = requests.delete(requestURL, json=requestJSON).json()
    if not r["status"] == "OK":
        raise IOError(str(r))

# Only works in Zeppelin >0.7
def run_paragraph(paragraph):
    (notebook, paragraph_id) = paragraph
    requestURL = "{}/api/notebook/run/{}/{}".format(host, notebook, paragraph_id)
    print(requestURL)
    return requests.post(requestURL) #.json()

def run_paragraph_async(paragraph):
    (notebook, paragraph_id) = paragraph
    requestURL = "{}/api/notebook/job/{}/{}".format(host, notebook, paragraph_id)
    return requests.post(requestURL) #.json()

def paragraph_status(paragraph):
    (notebook, paragraph_id) = paragraph
    requestURL = "{}/api/notebook/{}/paragraph/{}".format(host, notebook, paragraph_id)
    return requests.get(requestURL).json()["body"]["status"]

def paragraph_result(paragraph):
    (notebook, paragraph_id) = paragraph
    requestURL = "{}/api/notebook/{}/paragraph/{}".format(host, notebook, paragraph_id)
    r = requests.get(requestURL)
    try:
        # TODO: results in 0.7 and result in 0.6???????
        return r.json()["body"]["results"]
    except KeyError as e:
        print(str(r))
        print(str(r.json()))
        raise e

def run_paragraph_old(paragraph):
    (notebook, paragraph_id) = paragraph
    run_paragraph_async(paragraph)
    status = paragraph_status(paragraph)
    while status in ["RUNNING", "PENDING"]:
        status = paragraph_status(paragraph)
        time.sleep(1)
    if status in ["FINISHED", "ERROR"]:
        return paragraph_result(paragraph)
    else:
        print("Got {}".format(status))
        return None

def run_all(notebook):
    requestURL  = "{}/api/notebook/job/{}".format(host,notebook)
    r = requests.post(requestURL).json()
    if not r["status"] == "OK":
        raise IOError(str(r))

def clear_all(notebook):
    requestURL  = "{}/api/notebook/{}/clear".format(host,notebook)
    r = requests.put(requestURL).json()
    if not r["status"] == "OK":
        raise IOError(str(r))

try:
    n = new_notebook("Testbook")
    with open(args.source) as source_file:
        source_paragraph = add_paragraph(n, "Student code", source_file.read())

    source_result = run_paragraph_old(source_paragraph)

    source_messages = [ e.setdefault('data', "").strip() for e in source_result.setdefault('msg', {}) ]
    if source_result['code'] != 'SUCCESS':
        final_notebook = export_notebook(n)
        print(json.dumps({ "compiled" : False, "messages" : source_messages, "results" : [], "notebook" : final_notebook }))
    else:
        results = []
        for test_path in args.tests:
            with open(test_path) as test_file:
                test = json.load(test_file)
                test_paragraph = add_paragraph(n, test["title"], test["text"])
                test_result = run_paragraph_old(test_paragraph)
                test_messages = [ e.setdefault('data', "").strip() for e in test_result.setdefault('msg', {}) ]
                results.append({ "success": test_result['code'] == 'SUCCESS', "messages" : test_messages, "test" : test })
        final_notebook = export_notebook(n)
        print(json.dumps({ "compiled" : True, "messages" : source_messages, "results" : results, "notebook" : final_notebook }))

finally:
    delete_notebook(n)
